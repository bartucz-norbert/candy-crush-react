# Basic Candy Crush App with React 

It's a really simple candy crush app. I made with react framework just for fun, and for practice.
I hope you enjoy it :) 

After when you pull my app, you should run 

### `npm install`

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

